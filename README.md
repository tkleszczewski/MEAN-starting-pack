# MEAN starting pack

### [Angular](https://angular.io/)/[Express](http://expressjs.com)/[MongoDb](https://www.mongodb.com/) using [Mongoose](http://mongoosejs.com/docs/guide.html) as an ORM  

## Development commands  

#### Starting express server
  - (yarn/npm run) start-back
#### Starting angular app on default port (4200)
  - (yarn/npm run) start-front

## Build commands  
#### Build a dist folder and run express API
  - (yarn/npm run) build-prod
  
  
  
  
## Mongo setup (MacOS, Linux)
#### Download mongodb 
  - from **[mongo](https://www.mongodb.com/download-center#community)**  
#### Extract files
  - put them in '~' directory and rename it mongo
#### Create a dir for data  
  - cd ~ && mkdir mongo-data
#### Navigate from terminal to mongo/bin directory
  - cd ~/mongo/bin  
#### Run mongo
  - ./mongod --dbpath ~/mongo-data
  
### For Mongo GUI use [robo3t](https://robomongo.org/download);
