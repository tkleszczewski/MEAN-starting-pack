const express = require('express');
const {Note} = require('../models/note.model');

const notesRouter = express.Router();

notesRouter.post('/', (req, res) => {
    const {title, description} = req.body;
    
    var note = new Note({
        title,
        description
    });

    note.save().then(
        (doc) => {
            res.json(doc);
        },
        (err) => {
            res.status(400).send(err);
        }
    );
});

notesRouter.get('/', (req, res) => {
    Note.find().then(
        (doc) => {
            res.json(doc);
        },
        (err) => {
            res.status(400).send(err);
        }
    );
});

notesRouter.put('/', (req, res) => {
    const {title, description, _id} = req.body;

    Note.findByIdAndUpdate(_id, {
        title,
        description
    }).then(
        (doc) => {
            res.json(doc);
        },
        (err) => {
            res.status(400).send(doc);
        }
    );
});

notesRouter.delete('/', (req, res) => {
    const {_id} = req.body;

    Note.findByIdAndRemove(_id).then(
        (doc) => {
            res.json(doc);
        },
        (err) => {
            res.status(400).send(err);
        }
    );
});

module.exports = {notesRouter};