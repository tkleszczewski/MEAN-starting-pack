const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const {notesRouter} = require('./routes/note.routes');

const PORT = process.env.PORT || 3000;

const app = express();

app.use(express.static(path.join(__dirname, '../dist')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api/notes', notesRouter);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/index.html'));
});

app.listen(PORT, () => {
    console.log(`Server started
    Running on port: ${PORT}`);
});