const {mongoose} = require('../db/mongoose');

const schema = mongoose.Schema;

const Note = new schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: ''
    }
});

module.exports.Note = mongoose.model('Note', Note);